<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/nicolabiagioni/
 * @since      1.0.0
 *
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/admin/partials
 */

?>

<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
	<h2>iubenda Consent Solution for WordPress <?php esc_html_e( 'Options', $this->plugin_name ); ?></h2>

	<form method="post" name="cleanup_options" action="options.php">
	<?php
		// Grab all options.
		$options = get_option( $this->plugin_name );

		$secret_key = ( isset( $options['secret_key'] ) && ! empty( $options['secret_key'] ) ) ? esc_attr( $options['secret_key'] ) : 'Secret API Key';

		settings_fields( $this->plugin_name );
		do_settings_sections( $this->plugin_name );
		/**
		 * Sources
		 * http://searchengineland.com/tested-googlebot-crawls-javascript-heres-learned-220157
		 * http://dinbror.dk/blog/lazy-load-images-seo-problem/
		 * https://webmasters.googleblog.com/2015/10/deprecating-our-ajax-crawling-scheme.html
		 */
	?>

	<!-- Text -->
	<fieldset>
		<p><?php esc_html_e( 'Insert your Secret API Key.', $this->plugin_name ); ?></p>
		<legend class="screen-reader-text">
			<span><?php esc_html_e( 'Secret API Key', $this->plugin_name ); ?></span>
		</legend>
		<input type="text" class="secret_key" id="<?php esc_html_e( $this->plugin_name ); ?>-secret_key" name="<?php esc_html_e( $this->plugin_name ); ?>[secret_key]" value="<?php ! empty( $secret_key ) ? esc_html_e( $secret_key ) : esc_html_e( 'Secret API Key' ); ?>"/>
	</fieldset>

	<?php submit_button( __( 'Save all changes', $this->plugin_name ), 'primary', 'submit', true ); ?>
	</form>

</div>
