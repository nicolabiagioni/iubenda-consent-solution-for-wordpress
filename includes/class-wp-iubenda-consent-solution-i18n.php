<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bitbucket.org/nicolabiagioni/
 * @since      1.0.0
 *
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/includes
 * @author     Nicola Biagioni <nicola.biagioni@gmail.com>
 */
class Wp_Iubenda_Consent_Solution_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-iubenda-consent-solution',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
