<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/nicolabiagioni/
 * @since      1.0.0
 *
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/includes
 * @author     Nicola Biagioni <nicola.biagioni@gmail.com>
 */
class Wp_Iubenda_Consent_Solution_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
