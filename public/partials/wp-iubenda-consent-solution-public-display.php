<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/nicolabiagioni/
 * @since      1.0.0
 *
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
