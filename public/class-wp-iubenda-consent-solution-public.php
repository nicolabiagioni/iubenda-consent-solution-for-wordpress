<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bitbucket.org/nicolabiagioni/
 * @since      1.0.0
 *
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wp_Iubenda_Consent_Solution
 * @subpackage Wp_Iubenda_Consent_Solution/public
 * @author     Nicola Biagioni <nicola.biagioni@gmail.com>
 */
class Wp_Iubenda_Consent_Solution_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string $plugin_name The name of the plugin.
	 * @param    string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Iubenda_Consent_Solution_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Iubenda_Consent_Solution_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-iubenda-consent-solution-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Iubenda_Consent_Solution_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Iubenda_Consent_Solution_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-iubenda-consent-solution-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register customer fields
	 *
	 * @since 1.0.0
	 */
	public function register_customer_fields() {

		woocommerce_form_field( 'billing_first_name', array(
			'type'         => 'text',
			'label'        => __( 'First Name' ),
			'required'     => true,
			'autocomplete' => true,
			'class'        => array( 'woocommerce-form-row woocommerce-form-row--wide form-row form-row-first' ),
			'input_class'  => array( 'woocommerce-Input woocommerce-Input--text input-text' ),
		) );

		woocommerce_form_field( 'billing_last_name', array(
			'type'         => 'text',
			'label'        => __( 'Last Name' ),
			'required'     => true,
			'autocomplete' => true,
			'class'        => array( 'woocommerce-form-row woocommerce-form-row--wide form-row form-row-last' ),
			'input_class'  => array( 'woocommerce-Input woocommerce-Input--text input-text' ),
		) );

		if ( function_exists( 'wp_nonce_field' ) ) {
			wp_nonce_field( $this->plugin_name . '-submit' );
		}

	}

	/**
	 * Validate customer fields
	 *
	 * @since 1.0.0
	 * @param string $username Username.
	 * @param string $email Email.
	 * @param mixed  $validation_errors Validation errors.
	 */
	public function validate_customer_fields( $username, $email, $validation_errors ) {

		check_admin_referer( $this->plugin_name . '-submit' );

		if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
			$validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
		}
		if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
			$validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
		}

	}

	/**
	 * Save customer fields
	 *
	 * @since 1.0.0
	 * @param int $customer_id Customer ID.
	 */
	public function save_customer_fields( $customer_id ) {

		check_admin_referer( $this->plugin_name . '-submit' );

		if ( isset( $_POST['billing_first_name'] ) ) {
			// WordPress default first name field.
			update_user_meta( $customer_id, 'first_name', sanitize_text_field( wp_unslash( $_POST['billing_first_name'] ) ) );
			// WooCommerce billing first name.
			update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( wp_unslash( $_POST['billing_first_name'] ) ) );
		}
		if ( isset( $_POST['billing_last_name'] ) ) {
			// WordPress default last name field.
			update_user_meta( $customer_id, 'last_name', sanitize_text_field( wp_unslash( $_POST['billing_last_name'] ) ) );
			// WooCommerce billing last name.
			update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( wp_unslash( $_POST['billing_last_name'] ) ) );
		}

	}

	/**
	 * Register privacy fields
	 *
	 * @since 1.0.0
	 * @param string $text Default WooCOmmerce Privacy text. Search for woocommerce_get_privacy_policy_text in woocommerce/includes/wc-template-functions.php.
	 */
	public function register_privacy_fields_filter( $text ) {

		$checkbox = woocommerce_form_field( 'privacy_policy_checkbox', array(
			'type'         => 'checkbox',
			'label'        => wc_replace_policy_page_link_placeholders( $text ),
			'required'     => true,
			'autocomplete' => true,
			'class'        => array( 'woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide' ),
			'label_class'  => array( 'woocommerce-form__label woocommerce-form__label-for-checkbox checkbox' ),
			'input_class'  => array( 'woocommerce-form__input woocommerce-form__input-checkbox input-checkbo' ),
		) );

		return $checkbox;

	}

	/**
	 * Validate privacy fields
	 *
	 * @since 1.0.0
	 * @param string $username Username.
	 * @param string $email Email.
	 * @param string $validation_errors Validation errors.
	 */
	public function validate_privacy_fields( $username, $email, $validation_errors ) {

		check_admin_referer( $this->plugin_name . '-submit' );

		if ( isset( $_POST['privacy_policy_checkbox'] ) && empty( $_POST['privacy_policy_checkbox'] ) ) {
			$validation_errors->add( 'privacy_policy_checkbox_error', __( '<strong>Error</strong>: Privacy Policy checkbox is required!', 'woocommerce' ) );
		}

	}

	/**
	 * Save privacy fields
	 *
	 * @since 1.0.0
	 * @param int $customer_id Customer ID.
	 */
	public function save_privacy_fields( $customer_id ) {

		check_admin_referer( $this->plugin_name . '-submit' );

		if ( isset( $_POST['privacy_policy_checkbox'] ) && empty( $_POST['privacy_policy_checkbox'] ) ) {
			// WooCommerce billing first name.
			update_user_meta( $customer_id, 'privacy_policy_checkbox', sanitize_text_field( wp_unslash( $_POST['privacy_policy_checkbox'] ) ) );
		}

	}

	/**
	 * Save Iubenda fields
	 *
	 * @since 1.0.0
	 * @param int $customer_id Customer ID.
	 */
	public function save_iubenda_fields( $customer_id ) {

		check_admin_referer( $this->plugin_name . '-submit' );

		if ( isset( $_POST['email'], $_POST['billing_first_name'], $_POST['billing_last_name'], $_POST['privacy_policy_checkbox'] ) ) {

			$consent_data = array(
				'subject'       => array(
					'email'      => sanitize_text_field( wp_unslash( $_POST['email'] ) ),
					'first_name' => sanitize_text_field( wp_unslash( $_POST['billing_first_name'] ) ),
					'last_name'  => sanitize_text_field( wp_unslash( $_POST['billing_last_name'] ) ),
				),
				'legal_notices' => array(
					array(
						'identifier' => 'privacy_policy',
					),
					array(
						'identifier' => 'cookie_policy',
					),
				),
				'proofs'        => array(
					array(
						'form' => 'registration_form',
					),
				),
				'preferences'   => array(
					'privacy_policy' => sanitize_text_field( wp_unslash( $_POST['privacy_policy_checkbox'] ) ),
					'cookie_policy'  => sanitize_text_field( wp_unslash( $_POST['privacy_policy_checkbox'] ) ),
				),
			);
		}

		$url = 'https://consent.iubenda.com/consent';

		$options = get_option('wp-iubenda-consent-solution');

		$key = $options['secret_key'];

		$args = array(
			'method'  => 'POST',
			'headers' => array(
				'Content-Type' => 'application/json',
				'ApiKey'       => $key,
			),
			'body'    => wp_json_encode( $consent_data ),
		);

		$result = wp_remote_post( $url, $args );

	}

}
